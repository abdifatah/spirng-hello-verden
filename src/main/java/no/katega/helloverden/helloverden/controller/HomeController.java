package no.katega.helloverden.helloverden.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping("/home")
    public String showHomePage(Model model) {

        // return home page
        return "index";
    }

    // Added comment
    // Yet another comment

    // These changes are made by contact-change branch
    // New contacts
    

    // Here i will implement register functionality
    // Done
}
